module github.com/muety/mailwhale

go 1.15

require (
	github.com/BurntSushi/toml v1.0.0 // indirect
	github.com/emersion/go-sasl v0.0.0-20211008083017-0b9dcfb154ac
	github.com/emersion/go-smtp v0.15.0
	github.com/emvi/logbuch v1.2.0
	github.com/felixge/httpsnoop v1.0.2 // indirect
	github.com/google/uuid v1.3.0
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/configor v1.2.1
	github.com/rs/cors v1.8.2
	github.com/timshannon/bolthold v0.0.0-20210913165410-232392fc8a6a
	golang.org/x/crypto v0.0.0-20220210151621-f4118a5b28e2
	golang.org/x/sys v0.0.0-20220209214540-3681064d5158 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
